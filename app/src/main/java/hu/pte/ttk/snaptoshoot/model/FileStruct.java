package hu.pte.ttk.snaptoshoot.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

public class FileStruct {

    // absolutePath without file name
    private String absolutePath;
    private String extension;
    private String fileName;

    private String createTime;
    private UUID uniqueId;

    public FileStruct(@NonNull final String absolutePath, @NonNull final String fileName, @NonNull final String dateFormat){
        init(absolutePath, fileName, dateFormat);
    }

    public FileStruct(@NonNull final File absolutePath, @NonNull final String fileName, @NonNull final String dateFormat){
        init(absolutePath.getAbsolutePath(), fileName, dateFormat);
    }

    private void init(@NonNull final String absolutePath, @NonNull final String fileName, @NonNull final String dateFormat){
        this.absolutePath = absolutePath;
        this.fileName = fileName;

        String[] splitFileName = this.fileName.split("\\.");
        this.extension = splitFileName[splitFileName.length-1];

        createTime = new SimpleDateFormat(dateFormat, Locale.getDefault()).format(new Date());

        uniqueId = UUID.randomUUID();
    }

    public String getAbsolutePath(){ return absolutePath; }

    public String getExtension(){ return extension; }

    public String getFileName(){ return fileName; }

    public String getCreateTime(){ return createTime; }

    public UUID getUniqueId(){ return uniqueId; }

    public String getFullPathWithCreateTime(){
        return this.absolutePath + "/" + getNameWithCreateTime();
    }

    public String getFullPathWithUniqueId(){
        return this.absolutePath + "/" + getNameWithUniqueId();
    }

    public String getNameWithCreateTime(){
        return this.createTime + '_' + this.fileName;
    }

    public String getNameWithUniqueId(){
        return this.uniqueId.toString() + '_' + this.fileName;
    }

    public static String removeExtension(String path){
        String[] splitPath = path.split("\\.");

        StringBuilder noExtension = new StringBuilder();
        for (int i = 0; i < splitPath.length-1; i++){
            noExtension.append(splitPath[i]).append(".");
        }

        return noExtension.toString();
    }


}
