package hu.pte.ttk.snaptoshoot.util;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;

public final class ByteUtil {

    private ByteUtil(){}

    public static byte[] getByteArrayFromFile(String filePath, int bufferSize) throws IOException {
        FileInputStream fis = new FileInputStream(filePath);

        byte[] buffer = new byte[bufferSize];
        ByteArrayOutputStream os = new ByteArrayOutputStream();

        int read;
        while((read = fis.read(buffer)) != -1) {
            os.write(buffer, 0, read);
        }
        fis.close();
        os.close();

        return os.toByteArray();
    }

    //convert short to byte
    public static byte[] short2byte(short[] sData) {
        int shortArrsize = sData.length;
        byte[] bytes = new byte[shortArrsize * 2];
        for (int i = 0; i < shortArrsize; i++) {
            bytes[i * 2] = (byte) (sData[i] & 0x00FF);
            bytes[(i * 2) + 1] = (byte) (sData[i] >> 8);
            sData[i] = 0;
        }
        return bytes;
    }

}
