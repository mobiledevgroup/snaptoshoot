package hu.pte.ttk.snaptoshoot.model;

public final class KeyVal<K,V> {

    private K key;
    private V val;

    public KeyVal(K key, V val){
        this.key = key;
        this.val = val;
    }

    public K getKey(){
        return key;
    }

    public V getVal(){
        return val;
    }

}
