package hu.pte.ttk.snaptoshoot.model;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import hu.pte.ttk.snaptoshoot.util.ByteUtil;

public final class WAVE {

    private final int SAMPLING_RATE;
    private final short CHANNEL;
    private final short ENCODING;

    public WAVE(int sampleRate, short numOfChannel, short bitsPerSample){
        this.SAMPLING_RATE = sampleRate;
        this.CHANNEL = numOfChannel;
        this.ENCODING = bitsPerSample;
    }

    public void pcmToWav(final byte[] rawData, final File waveFile) throws IOException {
        DataOutputStream output = null;
        try {
            output = new DataOutputStream(new FileOutputStream(waveFile));
            rawToWave(output, rawData);
            output.write(rawData);
        } finally {
            if (output != null) {
                output.close();
            }
        }
    }

    // https://stackoverflow.com/questions/37281430/how-to-convert-pcm-file-to-wav-or-mp3
    public void pcmToWav(final File rawFile, final File waveFile, boolean deleteRaw) throws IOException {

        byte[] rawData = new byte[(int) rawFile.length()];
        DataInputStream input = null;
        try {
            input = new DataInputStream(new FileInputStream(rawFile));
            input.read(rawData);
        } finally {
            if (input != null) {
                input.close();
            }
        }

        DataOutputStream output = null;
        try {
            output = new DataOutputStream(new FileOutputStream(waveFile));
            rawToWave(output, rawData);
            output.write(ByteUtil.getByteArrayFromFile(rawFile.getAbsolutePath(), 1024));
        } finally {
            if (output != null) {
                output.close();
            }
        }

        if(deleteRaw) rawFile.delete();
    }

    private void rawToWave(DataOutputStream output, byte[] rawData) throws IOException {
        // WAVE header
        // see http://soundfile.sapp.org/doc/WaveFormat/
        writeString(output, "RIFF");                 // chunk id
        writeInt(output, 36 + rawData.length);       // chunk size = 4 + (8 + SubChunk1Size) + (8 + SubChunk2Size)
        writeString(output, "WAVE");                 // format
        writeString(output, "fmt ");                 // subchunk 1 id
        writeInt(output, 16);                        // subchunk 1 size
        writeShort(output, (short) 1);                     // audio format (1 = PCM)
        writeShort(output, CHANNEL);              // number of channels (mono = 1, stereo = 2)
        writeInt(output, SAMPLING_RATE);                   // sample rate
        writeInt(output, SAMPLING_RATE * CHANNEL * ENCODING / 8);// byte rate = SampleRate * NumChannels * BitsPerSample/8
        writeShort(output, (short)(CHANNEL * ENCODING / 8));           // block align = NumChannels * BitsPerSample/8
        writeShort(output, ENCODING);                    // bits per sample
        writeString(output, "data");                 // subchunk 2 id
        writeInt(output, rawData.length);                  // subchunk 2 size = NumSamples * NumChannels * BitsPerSample/8
        // Audio data (conversion big endian -> little endian)
        short[] shorts = new short[rawData.length / 2];
        ByteBuffer.wrap(rawData).order(ByteOrder.LITTLE_ENDIAN).asShortBuffer().get(shorts);
        ByteBuffer bytes = ByteBuffer.allocate(shorts.length * 2);
        for (short s : shorts) {
            bytes.putShort(s);
        }
    }

    public static byte[] removeWavHeader(byte[] wav){
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        os.write(wav, 44, wav.length - 44);
        return os.toByteArray();
    }

    /*public static byte[] fullyReadFileToBytes(File f, int off) throws IOException {
        int size = (int) f.length();
        byte[] bytes = new byte[size];
        byte[] tmpBuff = new byte[size];

        FileInputStream fis = new FileInputStream(f);
        try {
            int read = fis.read(bytes, off, size);
            if (read < size) {
                int remain = size - read;
                while (remain > off) {
                    read = fis.read(tmpBuff, off, remain);
                    System.arraycopy(tmpBuff, off, bytes, size - remain, read);
                    remain -= read;
                }
            }
        } catch (IOException e) {
            throw e;
        } finally {
            fis.close();
        }

        return bytes;
    }*/

    public static void writeInt(final DataOutputStream output, final int value) throws IOException {
        output.write(value >> 0);
        output.write(value >> 8);
        output.write(value >> 16);
        output.write(value >> 24);
    }

    public static void writeShort(final DataOutputStream output, final short value) throws IOException {
        output.write(value >> 0);
        output.write(value >> 8);
    }

    public static void writeString(final DataOutputStream output, final String value) throws IOException {
        for (int i = 0; i < value.length(); i++) {
            output.write(value.charAt(i));
        }
    }
}
