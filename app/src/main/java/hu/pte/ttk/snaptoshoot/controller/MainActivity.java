package hu.pte.ttk.snaptoshoot.controller;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.AudioFormat;
import android.net.Uri;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import hu.pte.ttk.snaptoshoot.R;
import hu.pte.ttk.snaptoshoot.model.FileStruct;
import hu.pte.ttk.snaptoshoot.model.KeyVal;
import hu.pte.ttk.snaptoshoot.model.Player;
import hu.pte.ttk.snaptoshoot.model.Recorder;
import hu.pte.ttk.snaptoshoot.model.WAVE;
import hu.pte.ttk.snaptoshoot.util.CameraUtil;
import hu.pte.ttk.snaptoshoot.util.StorageUtil;

public class MainActivity extends AppCompatActivity {

    public static final String dateFormat = "yyyyMMdd_HHmmss";
    public static final int REQUEST_IMAGE_CAPTURE = 1;

    private static final String appDir = "SnapToShoot";
    private static final String recDir = "Recordings";

    private static final String picDir = Environment.DIRECTORY_PICTURES+"/SnapToShoot/";

    private static final String PCM = ".pcm";
    private static final String WAV = ".wav";

    private final KeyVal<Short, Integer> CHANNEL = new KeyVal<>((short)1, AudioFormat.CHANNEL_IN_MONO);
    private final KeyVal<Short, Integer> ENCODING = new KeyVal<>((short)16, AudioFormat.ENCODING_PCM_16BIT);
    private final int SAMPLING_RATE = 44100;

    private List<String> recordingList = new ArrayList<>();

    private Player audioPlayer = new Player();
    private Recorder audioRecorder =
            new Recorder(CHANNEL.getVal(),    // MONO 16, STEREO 12
                         ENCODING.getVal(), // 16 bit per sample (related to audio quantize)
                         SAMPLING_RATE,   // 44100 samples taken in a single second
                        2,
                        1024);

    private PermissionManager permissionManager = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        permissionManager = new PermissionManager(this, this.getApplicationContext());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PermissionManager.REQUEST_PERMISSION_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "PERMISSION GRANTED", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "PERMISSION DENIED", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void record(View view){
        if(!permissionManager.requestPermissions(Manifest.permission.RECORD_AUDIO, Manifest.permission.WRITE_EXTERNAL_STORAGE)){
            File path = Environment.getExternalStoragePublicDirectory(picDir);
            StorageUtil.createDirectory(path);

            FileStruct fileHelper = new FileStruct(path, "rec", dateFormat);
            String fullPath = fileHelper.getFullPathWithCreateTime();

            recordingList.add(fullPath);
            try {
                audioRecorder.startRecording(fullPath + PCM);
            } catch (IllegalStateException e) {
                e.printStackTrace();
            }
        }

    }

    public void stop(View view){
        if(audioRecorder.isRecording()) {
            audioRecorder.stopRecording();
            try {
                String path = recordingList.get(recordingList.size() - 1);
                WAVE wav = new WAVE(this.SAMPLING_RATE, this.CHANNEL.getKey(), this.ENCODING.getKey());

                wav.pcmToWav(new File(path + PCM), new File(path + WAV), true);
            } catch (IOException e) {
                Log.e("IO", "Error while converting PCM file to WAV!");
                e.printStackTrace();
            }
        }
    }

    public void playRecord(View view){
        if(!audioPlayer.isPlaying()) {
            try {
                audioPlayer.playAudio(recordingList.get(recordingList.size() - 1) + WAV);
            } catch (IOException | IndexOutOfBoundsException | IllegalArgumentException | IllegalStateException | SecurityException e) {
                Log.e("IO+Other", "Error while playing recorded audio!");
                e.printStackTrace();
            }
        }
    }

    public void stopRecord(View view){
        try {
            audioPlayer.stopAudio();
        } catch (IllegalStateException e){
            e.printStackTrace();
        }
    }

    public void openCamera(View view){
        if(!permissionManager.requestPermissions(Manifest.permission.CAMERA)) {
            if (CameraUtil.checkCameraHardware(getApplicationContext())) {
                Intent cameraIntent = CameraUtil.getBuiltInCameraIntent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (cameraIntent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(cameraIntent, CameraUtil.REQUEST_IMAGE_CAPTURE);
                }
            } else {
                Log.e("Camera", "Device has no camera!");
            }
        }

    }

    private Intent cameraIntent = null;
    // https://stackoverflow.com/questions/14421694/taking-pictures-with-camera-on-android-programmatically
    public void shootPhoto(View view) {

        if(!permissionManager.requestPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)){
            // Note: Placing image into Pictures directory gallery can easily find it, but placing it elsewhere may not work
            // Note: By default Camera app saves under DCIM/Camera
            FileStruct photo =
                    new FileStruct(Environment.getExternalStoragePublicDirectory(picDir), "sajt.jpg", dateFormat);
            StorageUtil.createDirectory(photo.getAbsolutePath());

            try {
                cameraIntent = CameraUtil.capture(getApplicationContext(),
                        photo.getAbsolutePath(),
                        photo.getNameWithCreateTime(),
                        MediaStore.ACTION_IMAGE_CAPTURE);
                if (cameraIntent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(cameraIntent, REQUEST_IMAGE_CAPTURE);
                }
            } catch (Exception e) {
                Log.e("IO", "Error while making an image!");
                e.printStackTrace();
            }
        }
    }


    // Method is called when startActivityForResult method is called
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) throws RuntimeException {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Log.i("Camera","Picture saved to: "+ cameraIntent.getParcelableExtra(MediaStore.EXTRA_OUTPUT) +"!");
            Uri imagePath = cameraIntent.getParcelableExtra(MediaStore.EXTRA_OUTPUT);
            this.sendBroadcast(CameraUtil.addPictureToGallery(imagePath.getPath()));
        } else {
            throw new RuntimeException("Exception while tried to place image to gallery!");
        }

    }

}
