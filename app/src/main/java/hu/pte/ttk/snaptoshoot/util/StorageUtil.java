package hu.pte.ttk.snaptoshoot.util;

import android.content.ContentResolver;
import android.graphics.Bitmap;
import android.os.Environment;
import android.provider.MediaStore;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.UUID;

public final class StorageUtil {

    private StorageUtil(){}

    /**
     * UUID.randomUUID - for collision-resistant file names (new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()); would also be a good idea)
     * @param directory - use Environment.XY
     * @param recName - name of the file
     * @return absolute filepath
     */
    public static String getExternalUniquePath(final String directory, final String recName){
        String uniqueRecName = "/" + UUID.randomUUID() + "_" + recName;
        return Environment.getExternalStoragePublicDirectory(directory) + uniqueRecName;
    }
    public static String getExternalDatePath(final String directory, final String recName){
        String uniqueRecName = "/" + UUID.randomUUID() + "_" + recName;
        return Environment.getExternalStoragePublicDirectory(directory) + uniqueRecName;
    }

    public static boolean createDirectory(String fullPath){
        File newDir = new File(fullPath);

        // no need for .exists() because isDirectory also checks for it
        if (!newDir.isDirectory()) {
            newDir.mkdirs();
            return true;
        }

        return false;
    }
    public static boolean createDirectory(File newDir){

        // no need for .exists() because isDirectory also checks for it
        if (!newDir.isDirectory()) {
            newDir.mkdirs();
            return true;
        }

        return false;
    }

    public static void placePhotoToGallery(ContentResolver contentResolver, Bitmap bitmap, String title, String desc) throws FileNotFoundException {
        MediaStore.Images.Media.insertImage(contentResolver, bitmap, title , desc);
    }

    public static void placePhotoToGallery(ContentResolver contentResolver, String path, String title, String desc) throws FileNotFoundException {
        MediaStore.Images.Media.insertImage(contentResolver, path, title , desc);
    }

    public static String removeFileFromPath(final String imagePath) {
        return imagePath.replaceFirst("file://", "");
    }
}
