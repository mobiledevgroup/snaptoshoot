package hu.pte.ttk.snaptoshoot.model;

import android.media.AudioRecord;
import android.media.MediaPlayer;
import android.util.Log;

import java.io.IOException;

public final class Player{

    private MediaPlayer mediaPlayer;

    public boolean isPlaying(){
        return (mediaPlayer != null) && mediaPlayer.isPlaying();
    }

    public void playAudio(String audioPath) throws IOException, IllegalStateException, IllegalArgumentException, SecurityException {
        mediaPlayer = new MediaPlayer();
        // Throws IOException
        mediaPlayer.setDataSource(audioPath);
        mediaPlayer.prepare();
        mediaPlayer.start();

        Log.i("Player", "Audio is playing: " + audioPath);

        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                Log.i("Player", "Finished playing audio!");
                stopAudio();
            }
        });
    }

    public void stopAudio() throws IllegalStateException{
        // Exits if object is null or audio is not playing
        if(mediaPlayer == null) { return; }
        if(mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
            Log.i("Player", "Audio playing interrupted!");
        }
        mediaPlayer.reset();
        mediaPlayer.release();
        mediaPlayer = null;
    }


}
